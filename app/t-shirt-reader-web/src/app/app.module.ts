import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { WebcamModule } from 'ngx-webcam';
import { ShirtTextComponent } from './shirt-text/shirt-text.component';
import { LanguageChoiceComponent } from './language-choice/language-choice.component';

@NgModule({
  declarations: [
    AppComponent,
    ShirtTextComponent,
    LanguageChoiceComponent
  ],
  imports: [
    BrowserModule,
    WebcamModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
