import logo from './logo.svg';
import './App.css';
import WebcamCapture from './webcam/WebCam';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          This is for React Reading T-Shirts
        </p>
        <a href="https://gitlab.com/create_art/t_shirt_reader/-/tree/main/app/t-shirt-reader-react">App Source</a>
      </header>
      <WebcamCapture/>
    </div>
  );
}

export default App;
