import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app source link', () => {
  render(<App />);
  const linkElement = screen.getByText(/app source/i);
  expect(linkElement).toBeInTheDocument();
});
