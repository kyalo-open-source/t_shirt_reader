import { render, screen } from '@testing-library/react';
import WebcamCapture from './WebCam';

// TODO: Add act https://reactjs.org/docs/test-utils.html#act

test('renders capture button', () => {
    render(<WebcamCapture />);
    const buttonElement = screen.getByText(/capture photo/i);
    expect(buttonElement).toBeInTheDocument();
  });