"""
Fixtures to reused test data
"""
import pytest

@pytest.fixture
def manual_str():
    line_data = open("tests/data/aws_ascii.txt").readlines()
    test_data = " ".join(line_data)
    test_data = test_data.replace("\n","")
    return test_data