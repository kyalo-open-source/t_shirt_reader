# T Shirt Reader

End goal is to make a mobile application that can read T-Shirts, capture unique ones and catalog where to source them.

Initial inspiration from Amazon T-Shirt with binary ascii code and of course my obsession with making them. Some functions are akin to:
- Google Lens - Convert images to text and perform web searches
- Office Lens - Lock onto the human T-shape instead of the rectangle

Also as a market analysis catalog due to the cost of making these ephemera which are expensive on resources.

## T-Shirt Sources
- [Redbubble](https://www.redbubble.com/g/t-shirts)
- [Zazzle](https://www.zazzle.com/s/t-shirt)
- [Threadless](https://www.threadless.com/search/?sort=popular&style=t-shirt&type=regular)

## T-Shirt Books
- [1000 T-Shirts that make a statement](https://www.amazon.com/1000-T-Shirts-That-Make-Statement/dp/0789332795)